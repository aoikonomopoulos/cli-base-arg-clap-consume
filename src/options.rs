use super::*;
lazy_static! {
    pub static ref FOO : base_arg::BaseArg<&'static str> = base_arg::BaseArg {
        forms: vec!["-f", "--foo"],
    };
    pub static ref BAR : base_arg::BaseArg<&'static str> = base_arg::BaseArg {
        forms: vec!["+[num]"],
    };
}
