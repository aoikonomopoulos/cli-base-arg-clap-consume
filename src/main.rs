extern crate base_arg;
extern crate clap;
#[macro_use]
extern crate lazy_static;

use clap::{App, Arg};
mod options;

fn main() {
    let app = App::new("prog")
        .version("0.1")
        .arg(Arg::with_name("foo")
             .according_to(&options::FOO));
        // This argument starts with a '+' and therefore adding it
        // to clap will fail at runtime.
        // .arg(Arg::with_name("bar")
        //      .according_to(&options::BAR));
    let matches = app.get_matches();
    println!("{:?}", matches);
}
