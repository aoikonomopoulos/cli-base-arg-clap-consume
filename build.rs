extern crate base_arg;
#[macro_use]
extern crate lazy_static;
extern crate roff;

#[path = "src/options.rs"]
mod options;

use std::env;
use std::fs;
use std::io::{self, Write};
use std::path::Path;
use roff::*;

fn main() {
    // Here we have complete control over the formatting of the manpage,
    // but can still make use of helpers to generate the formatting for
    // arguments, if we so choose.
    let page = Roff::new("prog", 1)
        .section("name", &["prog descr"])
        .section("synopsis", &["TBD"])
        .section("description", &["TBD"])
        .section("options", &[
            list(
                &[form_list(&options::FOO)],
                &["Performs foo", "\n"],
            ),
            list(
                &[form_list(&options::BAR)],
                &["Specifies num", "\n"]
            ),
        ]);
    write_manpage(&page, "prog.1")
        .expect("Couldn't write man page")
}

fn form_list<T>(ba : &base_arg::BaseArg<T>) -> String
where
    T: AsRef<str> + Clone,
{
    let opts : Vec<String> = ba.forms.iter().map(|f| { bold(f.as_ref())}).collect();
    opts.join(", ")
}

fn write_manpage(page : &Roff, filename : &str) -> io::Result<()> {
    let outdir = env::var_os("OUT_DIR").expect("No OUT_DIR available");
    fs::create_dir_all(&outdir).expect("Error creating output directory");
    let pagepath = Path::new(&outdir).join(filename);
    fs::File::create(&pagepath)?
        .write_all(&page.render().as_bytes())
}
